//
// Created by Russell Brown on 19/12/2019.
//

#ifndef GALAXY_COORDINATES_H
#define GALAXY_COORDINATES_H


class coordinates {
public:
    coordinates(double x, double y, double z);

public:
    double x;
    double y;
    double z;

};

coordinates::coordinates(double x, double y, double z) : x(x), y(y), z(z) {}

#endif //GALAXY_COORDINATES_H
