//
// Created by Russell Brown on 19/12/2019.
//



#define GALAXY_STAR_H

#include <string>
#include "RGBColor.h"

using namespace std;

class Star {
public:

    Star();

    int order;
    string starId;
    string starType;
    string starName;
    double luminosity;
    double radius;


    double mass;
    double stellaCharacteristicsSeed;

    double tempKelvin;

    int getOrder() const {
        return order;
    }

    void setOrder(int order) {
        Star::order = order;
    }

    const string &getStarId() const {
        return starId;
    }

    void setStarId(const string &starId) {
        Star::starId = starId;
    }

    const string &getStarType() const {
        return starType;
    }

    void setStarType(const string &starType) {
        Star::starType = starType;
    }

    const string &getStarName() const {
        return starName;
    }

    void setStarName(const string &starName) {
        Star::starName = starName;
    }

    double getLuminosity() const {
        return luminosity;
    }

    void setLuminosity(double luminosity) {
        Star::luminosity = luminosity;
    }

    double getRadius() const {
        return radius;
    }

    void setRadius(double radius) {
        Star::radius = radius;
    }

    double getMass() const {
        return mass;
    }

    void setMass(double mass) {
        Star::mass = mass;
    }

    double getStellaCharacteristicsSeed() const {
        return stellaCharacteristicsSeed;
    }

    void setStellaCharacteristicsSeed(double stellaCharacteristicsSeed) {
        Star::stellaCharacteristicsSeed = stellaCharacteristicsSeed;
    }

    double getTempKelvin() const {
        return tempKelvin;
    }

    void setTempKelvin(double tempKelvin) {
        Star::tempKelvin = tempKelvin;
    }

    coordinates *getStarCoordinates() const {
        return starCoordinates;
    }

    void setStarCoordinates(coordinates *starCoordinates) {
        Star::starCoordinates = starCoordinates;
    }

    coordinates *getGalaxyCoordinates() const {
        return galaxyCoordinates;
    }

    void setGalaxyCoordinates(coordinates *galaxyCoordinates) {
        Star::galaxyCoordinates = galaxyCoordinates;
    }

    RGBColor *getRgb() const {
        return rgb;
    }

    void setRgb(RGBColor *rgb) {
        Star::rgb = rgb;
    }

    int getPlanetAmount() const {
        return planetAmount;
    }

    void setPlanetAmount(int planetAmount) {
        Star::planetAmount = planetAmount;
    }

    coordinates* starCoordinates;
    coordinates* galaxyCoordinates;
    RGBColor* rgb;
    int planetAmount;
};

Star::Star() {

}

