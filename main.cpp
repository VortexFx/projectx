#include <iostream>
#include <cmath>
#include <algorithm>
#include "Seed.h"
#include <iomanip>
#include <list>
#include "MD5.h"
#include "coordinates.h"
#include "Star.h"
#include "RGBColor.h"
#include "Planet.h"
#include <vector>

using namespace std;

vector<Star*> generateStars(int x, int y, int z) {

    vector<Star*> stars;
    Star* star = new Star();
    star->setStarId("015195ddcbe1003308edbcc9c89e5aa1");
    stars.push_back(star);

    return stars;

}

Star* getStar(string starId, int x, int y, int z) {
    vector<Star*> stars = generateStars(x, y, z);

    for(int i = 0; i< stars.size(); i++) {
        if(stars[i]->getStarId() == starId){
            return stars[i];
        }
    }

    return nullptr;
}

vector<Planet*> generatePlanets(string starId, int x, int y, int z) {

    vector<Planet*> planets;

    return planets;
}

bool checkStarExists(const string& hash, int x, int y, int z) {

    vector<Star*> stars = generateStars(x, y, z);

    for(int i = 0; i< stars.size(); i++) {
        if(stars[i]->getStarId() == hash){
            cout << "true : " << stars[i]->getStarId() << " == " << hash << std::endl;
            return true;
        }
    }

    cout << "false not found:  " << hash << std::endl;
    return false;
}

vector<string> getRandomStars(int x, int y, int z) {
    vector<string> starIds;

    return starIds;
}

vector<string> getHabitablePlanet(vector<string> starIds, int x, int y, int z) {
    vector<string> planetIds;

    return planetIds;
}



int main() {

    checkStarExists("015195ddcbe1003308edbcc9c89e5aa1", 10,10,10);

    return 0;
}
