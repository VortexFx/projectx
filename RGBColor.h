//
// Created by Russell Brown on 19/12/2019.
//

#ifndef GALAXY_RGBCOLOR_H
#define GALAXY_RGBCOLOR_H


class RGBColor {
public:

public:
    double r;
    double g;
    double b;

    double getR() const {
        return r;
    }

    void setR(double r) {
        RGBColor::r = r;
    }

    double getG() const {
        return g;
    }

    void setG(double g) {
        RGBColor::g = g;
    }

    double getB() const {
        return b;
    }

    void setB(double b) {
        RGBColor::b = b;
    }
};


#endif //GALAXY_RGBCOLOR_H
