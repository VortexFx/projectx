//
// Created by Russell Brown on 18/12/2019.
//

#ifndef GALAXY_SEED_H
#define GALAXY_SEED_H


class Seed {
public:
    double value;
    Seed(double value);
    double GetValue();
    ~Seed();
};


#endif //GALAXY_SEED_H
