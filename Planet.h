//
// Created by Russell Brown on 19/12/2019.
//
#include <string>

using namespace std;

class Planet {

public:

    string planetId;

    double distanceAu;

    string massKg;

    double radiusKm;

    bool habitableZone = false;

    string type;

    const string &getPlanetId() const {
        return planetId;
    }

    void setPlanetId(const string &planetId) {
        Planet::planetId = planetId;
    }

    double getDistanceAu() const {
        return distanceAu;
    }

    void setDistanceAu(double distanceAu) {
        Planet::distanceAu = distanceAu;
    }

    const string &getMassKg() const {
        return massKg;
    }

    void setMassKg(const string &massKg) {
        Planet::massKg = massKg;
    }

    double getRadiusKm() const {
        return radiusKm;
    }

    void setRadiusKm(double radiusKm) {
        Planet::radiusKm = radiusKm;
    }

    bool isHabitableZone() const {
        return habitableZone;
    }

    void setHabitableZone(bool habitableZone) {
        Planet::habitableZone = habitableZone;
    }

    const string &getType() const {
        return type;
    }

    void setType(const string &type) {
        Planet::type = type;
    }

    bool isColonised() const {
        return colonised;
    }

    void setColonised(bool colonised) {
        Planet::colonised = colonised;
    }

    const string &getPlanetName() const {
        return planetName;
    }

    void setPlanetName(const string &planetName) {
        Planet::planetName = planetName;
    }

    double getVelocity() const {
        return velocity;
    }

    void setVelocity(double velocity) {
        Planet::velocity = velocity;
    }

    double getOrbitalRadiusOfPlanet() const {
        return orbitalRadiusOfPlanet;
    }

    void setOrbitalRadiusOfPlanet(double orbitalRadiusOfPlanet) {
        Planet::orbitalRadiusOfPlanet = orbitalRadiusOfPlanet;
    }

    double getOrbitalPeriod() const {
        return orbitalPeriod;
    }

    void setOrbitalPeriod(double orbitalPeriod) {
        Planet::orbitalPeriod = orbitalPeriod;
    }

    int getUserId() const {
        return userId;
    }

    void setUserId(int userId) {
        Planet::userId = userId;
    }

    bool colonised = false;

    string planetName;

    double velocity;

    double orbitalRadiusOfPlanet;

    double orbitalPeriod;

    int userId;

};

