//
// Created by Russell Brown on 18/12/2019.
//

#include "Seed.h"
#include <cmath>

Seed::Seed(double value)
{
    this->value = value;
}

double Seed::GetValue() {
    this->value = sin(this->value) * 10000;
    return this->value - floor(this->value);
}

Seed::~Seed()
{
}
